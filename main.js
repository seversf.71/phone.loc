var demo = new Vue({
    el: '#main',
    data: {
        searchString: "",

        // Модель данных. Эти данные в другом случае передавались бы ajax'ом, но сейчас описаны прямо здесь для упрощения.

        tel: "sms:+79041438128?body=Пожалуйста добавьте номер с справочник ...",
        ban: "Отправить СМС для добавления номера в справочник (жмите сюда)"
        ,
        articles: [
            {
                "title": "Изготовление сайтов, интернет-магазинов",
                "url": "tel:+79041438128",
                "image": "images/nudinsk.webp"
            },
            {
                "title": "Аварийная служба (за Тройкой)",
                "url": "tel:+73955770261",
                "image": "images/avariika_1.webp"
            },
            {
                "title": "Автостанция",
                "url": "tel:+79086449383",
                "image": "images/no_image.webp"
            },
            {
                "title": "'Байт'",
                "url": "tel:+73955771831",
                "image": "images/bite.webp"
            },
            {
                "title": "Детская поликлиника",
                "url": "tel:+73955758212",
                "image": "images/poliklinika.webp"
            },
            {
                "title": "автосервис 'Авто-Швед'",
                "url": "tel:+79501167177",
                "image": "images/auto_shved.webp"
            },
            {
                "title": "Налоговая Нижнеудинск",
                "url": "tel:+73955770442",
                "image": "images/nalog.webp"
            },
            {
                "title": "Такси 'ЛЕДИ'",
                "url": "tel:+79500878318",
                "image": "images/taxi.webp"
            },
            {
                "title": "Шторы Ламбрикены",
                "url": "tel:+79149450597",
                "image": "images/shtors.webp"
            },
            {
                "title": "Транспортная компания 'Энергия'",
                "url": "tel:+79500993966",
                "image": "images/energy.webp"
            },
            {
                "title": "Глава Нижнеудинска Путов А.В",
                "url": "tel:+73955770154",
                "image": "images/administracia.webp"
            },
            {
                "title": "Первый зам главы Нижнеудинска Мускаев Ю.Н",
                "url": "tel:+73955770372",
                "image": "images/administracia.webp"
            },
            {
                "title": "Хлебозавод на Советской",
                "url": "tel:+73955771291",
                "image": "images/broad.webp"
            },
            {
                "title": "Приют 'Выход есть'",
                "url": "tel:+79247070465",
                "image": "images/shelter.webp"
            },
            {
                "title": "Такси 'Алёна'",
                "url": "tel:+79086652222",
                "image": "images/taxi.webp"
            },
            {
                "title": "НеТакси 'На крайний случай'",
                "url": "tel:+79041438128",
                "image": "images/no_taxi.webp"
            }
        ],
        items:[
            {
                "bimg": "images/kod.webp",
                "text": "Обучение Веб-разработке",
                "link": "https://nudinsk.com/lessons.html"
            },
            {
                "bgc": "orange",
                "text": "Ваш баннер #2",
                "link": "tel:+79041438128"
            },
            {
                "bgc": "orange",
                "text": "Ваш баннер #3",
                "link": "tel:+79041438128"
            },
            {
                "bgc": "orange",
                "text": "Ваш баннер #4 ",
                "link": "tel:+79041438128"
            }
        ],
        orange: 'orange',
        green: 'green'
    },
    computed: {
        // Вычисленное свойство, которое содержит только те статьи, которые соответствуют searchString.
        filteredArticles: function () {
            var articles_array = this.articles,
                searchString = this.searchString;

            if(!searchString){
                return articles_array;
            }

            searchString = searchString.trim().toLowerCase();

            articles_array = articles_array.filter(function(item){
                if(item.title.toLowerCase().indexOf(searchString) !== -1){
                    return item;
                }
            })

            // Возвращает массив с отфильтрованными данными.
            return articles_array;;
        }
    }
});
